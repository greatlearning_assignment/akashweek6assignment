package main;

import java.util.Scanner;

import DesignPattern.MoviesFactory;

/*
 * Test class for Movies On Tips.............
 */
public class MoviesTest {

	public static void main(String[] args) {

		boolean flag = true;

		do {

			System.out.println();

			System.out.println("1.List of Movies Comming");

			System.out.println("2.List of Top Rated Movie");

			System.out.println("3.List of Favourite Movie");

			System.out.println("4.Exit");

			@SuppressWarnings("resource")
			Scanner con = new Scanner(System.in);

			int choice = con.nextInt();

			//			Create Object of MovieFactory class........
			MoviesFactory moviesFactory = new MoviesFactory();


			switch(choice) {

			case 1:

				moviesFactory.getMovies(choice);

				break;

			case 2:

				moviesFactory.getMovies(choice);

				break;

			case 3:

				moviesFactory.getMovies(choice);

				break;

			case 4:

				flag = false;

				break;

			default:

				System.out.println("Wrong input.........");

			}     

		}while(flag);

	}

}