package DesignPattern;

import java.sql.SQLException;

import database.ComingMovieDatabase;
import database.FavouriteMoviesDatabase;
import database.TopRatedMovieDatabase;

public class MoviesFactory {

	public static void getMovies(int category) {

		if (category == 1) {

			ComingMovieDatabase commingMovies = new ComingMovieDatabase();

			try {

				commingMovies.getCommingMovies();



			} catch (SQLException e) {

				// TODO Auto-generated catch block

				e.printStackTrace();

			}

		} else if (category == 2) {

			TopRatedMovieDatabase commingMovies = new TopRatedMovieDatabase();

			try {

				commingMovies.getTopRatedMovies();



			} catch (SQLException e) {

				// TODO Auto-generated catch block

				e.printStackTrace();

			}

		} else {

			FavouriteMoviesDatabase favouriteMoviesDatabase = new FavouriteMoviesDatabase();

			try {

				favouriteMoviesDatabase.getFavouriteMovies();



			} catch (SQLException e) {

				// TODO Auto-generated catch block

				System.out.println("Favourite movie list is empty");

				e.printStackTrace();

			}

		}

	}

}
