package DesignPattern;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class Movie_On_Tips {

	private int id;

	private String title;

	private double imdb;

	private String geners;

	private String actors;

	private String duration;

	private Date release_date;

	private String story_line;

	private String img;



	public Movie_On_Tips(int id, String title, double imdb, String geners, String actors, String duration, Date release_date,

			String story_line, String img) {

		super();

		this.id = id;

		this.title = title;

		this.imdb = imdb;

		this.geners = geners;

		this.actors = actors;

		this.duration = duration;

		this.release_date = release_date;

		this.story_line = story_line;

		this.img = img;

	}



	public abstract String getMovieId();



	public abstract String getMovieTitle();



	public abstract String getMovieIMDB();



	public abstract String getMovieGeners();



	public abstract String getMovieActors();



	public abstract String getMovieDuration();



	public abstract String getMovieReleaseDate();



	public abstract String getMovieStoryLine();



	public abstract String getMovieImage();



	public List<String> getMovieData() {

		List<String> movieData = new ArrayList<String>();

		movieData.add(getMovieId() + this.id);

		movieData.add(getMovieTitle() + this.title);

		movieData.add(getMovieIMDB() + this.imdb);

		movieData.add(getMovieGeners() + this.geners);

		movieData.add(getMovieActors() + this.actors);

		movieData.add(getMovieDuration() + this.duration);

		movieData.add(getMovieReleaseDate() + this.release_date);

		movieData.add(getMovieStoryLine() + this.story_line);

		movieData.add(getMovieImage() + this.img);

		return movieData;

	}



	public int getId() {

		return id;

	}



	public void setId(int id) {

		this.id = id;

	}



	public String getTitle() {

		return title;

	}



	public void setTitle(String title) {

		this.title = title;

	}



	public double getImdb() {

		return imdb;

	}



	public void setImdb(double imdb) {

		this.imdb = imdb;

	}



	public String getGeners() {

		return geners;

	}



	public void setGeners(String geners) {

		this.geners = geners;

	}



	public String getActors() {

		return actors;

	}



	public void setActors(String actors) {

		this.actors = actors;

	}



	public String getDuration() {

		return duration;

	}



	public void setDuration(String duration) {

		this.duration = duration;

	}



	public java.sql.Date getReleaseDate() {

		return (java.sql.Date) release_date;

	}



	public void setReleaseDate(Date release_date) {

		this.release_date = release_date;

	}



	public String getStoryLine() {

		return story_line;

	}



	public void setStoryLine(String story_line) {

		this.story_line = story_line;

	}



	public String getImg() {

		return img;

	}



	public void setImgPath(String img) {

		this.img = img;

	}



	@Override

	public String toString() {

		return "CommingMovies [id=" + id + ", title=" + title + ", imdb=" + imdb + ", geners=" + geners + ", actors="

                                                          + actors + ", duration=" + duration + ", releaseDate=" + release_date + ", storyLine=" + story_line

                                                          + ", img=" + img + "]";

	}



}