package DesignPattern;

import java.util.Date;

/*
 * Create Child class of Movie_On_Tips class.........
 */
public class FavouriteMovies extends Movie_On_Tips {

	public FavouriteMovies(int id, String title, double imdb, String geners, String actors, String duration,
			Date release_date, String story_line, String img) {
		super(id, title, imdb, geners, actors, duration, release_date, story_line, img);
		// TODO Auto-generated constructor stub
	}

	@Override

	public String getMovieId() {

		// TODO Auto-generated method stub

		return "Favourite movie id: ";

	}



	@Override

	public String getMovieTitle() {

		// TODO Auto-generated method stub

		return "Favourite movie title: ";

	}



	@Override

	public String getMovieIMDB() {

		// TODO Auto-generated method stub

		return "Favourite movie imdb rating: ";

	}



	@Override

	public String getMovieGeners() {

		// TODO Auto-generated method stub

		return "Favourite movie geners: ";

	}



	@Override

	public String getMovieActors() {

		// TODO Auto-generated method stub

		return "Favourite movie actors: ";

	}



	@Override

	public String getMovieDuration() {

		// TODO Auto-generated method stub

		return "Favourite movie duration: ";

	}



	@Override

	public String getMovieReleaseDate() {

		// TODO Auto-generated method stub

		return "Favourite movie release date: ";

	}



	@Override

	public String getMovieStoryLine() {

		// TODO Auto-generated method stub

		return "Favourite movie stroy line: ";

	}



	@Override

	public String getMovieImage() {

		// TODO Auto-generated method stub

		return "Favourite movie image path: ";

	}



}
