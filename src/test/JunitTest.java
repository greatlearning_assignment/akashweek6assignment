package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.SQLException;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import DesignPattern.CommingMovies;
import DesignPattern.FavouriteMovies;
import DesignPattern.MoviesFactory;
import DesignPattern.TopRatedMovie;
import database.ComingMovieDatabase;
import database.FavouriteMoviesDatabase;
import database.TopRatedMovieDatabase;

public class JunitTest {
	private int id = 5;
	private String name = "Black Panther";
	private String gener;
	private String story_line;
	private String duration;
	private String releaseDate;
	private String actors;
	private double imdb;
	private String movieTitle;
	
	
	@BeforeEach
	public void beforeEach() {
		id = 5;
		name = "Black Panther";
		gener = "Action";
		story_line = "";
		releaseDate = "2018-05-02";
		actors = "Chadwick Boseman";
		duration = "PT97M";
		imdb = 7.9;
	}
	
	@AfterEach
	public void afterEach() {
		id = 0;
		name = null;
		gener = null;
		story_line = null;
		duration = null;
		releaseDate = null;
		actors = null;
		imdb = (Double) null;
		movieTitle = "Aiyaary";
	}
	

	
	// Test Case for all getters and setters for the ComingSoonMovies class
	@Test
	public void getterAndSetterForComingSoonTest() {
//		ComingSoon comingSoon = new ComingSoon();
		CommingMovies comingSoon = new CommingMovies(id, actors, imdb, duration, gener, movieTitle, null, story_line, releaseDate);
//		
//		comingSoon.setId(id);
//		comingSoon.setActors(actors);
//		comingSoon.setTitle(name);
//		comingSoon.setDuration(duration);
//		comingSoon.setGeners(genre);
//		comingSoon.setReleaseDate(releaseDate);
//		comingSoon.setStoryLine(contentRating); 
//		comingSoon.setImdb(imdb);
//		
		assertTrue(comingSoon.getId() == id);
		assertTrue(comingSoon.getTitle().equals(name));
		assertTrue(comingSoon.getActors().equals(actors));
		assertTrue(comingSoon.getGeners().equals(gener));
		assertTrue(comingSoon.getReleaseDate().equals(releaseDate));
	}
	
	// Test Case for Getters and Setters for TheaterMovies class
	
//	private void assertTrue(boolean equals) {
//		// TODO Auto-generated method stub
//		
//	}

	@Test
	public void gettersAndSettersForTheaterMovies() {
		TopRatedMovie topMovies = new TopRatedMovie(id, actors, imdb, duration, gener, movieTitle, null, story_line, releaseDate);
		
		
		assertFalse(topMovies.getId() != id);
		assertFalse(!topMovies.getTitle().equals(name));
		assertFalse(!topMovies.getGeners().equals(gener));
		assertFalse(!topMovies.getDuration().equals(duration));
		assertFalse(!topMovies.getReleaseDate().equals(releaseDate));
		assertFalse(!topMovies.getActors().equals(actors));
		
	}
	
	
	// Test for FavoritesMovies Class
	@Test
	public void gettersAndSettersForFavoriteMovies() {
		FavouriteMovies favorites = new FavouriteMovies(id, actors, imdb, duration, gener, movieTitle, null, story_line, releaseDate);
		
		
		assertEquals(favorites.getId(),id);
		assertEquals(favorites.getTitle(),name);
		assertEquals(favorites.getGeners(),gener);
		assertEquals(favorites.getStoryLine() ,story_line);
		assertEquals(favorites.getDuration(),duration);
		assertEquals(favorites.getReleaseDate(),releaseDate);
		
	}
	
	// Test Case for Coming Movies Database Class List
	
	@Test
	public void comingMovieDatabaseTest() {
		
		ComingMovieDatabase comingMovieDatabase = new ComingMovieDatabase();
		
		System.out.println(comingMovieDatabase);
	}
	
	// Test Case for TopRated Movies Database Class List
	
	@Test
	public void topRatedMovieDatabaseTest() {
		
		TopRatedMovieDatabase topRatedMoviesDatabase = new TopRatedMovieDatabase();
		System.out.println(topRatedMoviesDatabase);
	}
	
	// Test for Favorite Movies Database class
	@Test
	public void favoriteMovieDatabaseTest() throws SQLException {
		
		FavouriteMoviesDatabase favoriteMoviesDatabase = new FavouriteMoviesDatabase();

		System.out.println(favoriteMoviesDatabase);
	}
}
