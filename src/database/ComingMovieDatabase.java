package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import DesignPattern.CommingMovies;
import DesignPattern.Movie_On_Tips;

public class ComingMovieDatabase {

	Connection connection = SingletonConnection.getConnection();


	// Create a method for fetching data from database.......
	public void getCommingMovies() throws SQLException {

		String sqlQuery = "Select * from coming_movies";



		Statement statement = connection.createStatement();

		ResultSet result = statement.executeQuery(sqlQuery);

		System.out.println("----------------- Comming movies details ------------------");

		System.out.println();



		if (result.getFetchSize() == 0) {

			while (result.next()) {

				int id = result.getInt(1);

				String title = result.getString(2);

				double imdb = result.getDouble(6);

				String geners = result.getString(5);

				String actors = result.getString(3);

				String duration = result.getString(4);

				java.sql.Date release_date = result.getDate(8);

				String story_line = result.getString(9);

				String img = result.getString(7);

				Movie_On_Tips commingMovie = new CommingMovies(id, title, imdb, geners, actors, duration, release_date,

						story_line, img);

				System.out.println(commingMovie.getMovieData());

			}
			System.out.println();

		}

		else {

			System.out.println();

			System.out.println("Comming movies list is empty!");

		}

	}
}

