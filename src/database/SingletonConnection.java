package database;
import java.sql.*;

public class SingletonConnection
{
	private static Connection conn;

	private SingletonConnection(){
		super();
	}
	/*
	 * Stablish connection with mysql database..........
	 */
	public static Connection getConnection()
	{
		try
		{
			if(conn==null)
			{
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movieontips","root","root");
				System.out.println("connected");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		return conn;
	}
}