package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import DesignPattern.Movie_On_Tips;
import DesignPattern.TopRatedMovie;

public class TopRatedMovieDatabase {

	Connection connection = SingletonConnection.getConnection();


	// Create a method for fetching data from database.......
	public void getTopRatedMovies() throws SQLException {

		String sqlQuery = "Select * from top_rated_movies";



		Statement statement = connection.createStatement();

		ResultSet result = statement.executeQuery(sqlQuery);

		System.out.println("----------- Top Rated movies details --------------");

		System.out.println();



		if (result.getFetchSize() == 0) {

			while (result.next()) {

				int id = result.getInt(1);

				String title = result.getString(2);

				double imdb = result.getDouble(6);

				String geners = result.getString(5);

				String actors = result.getString(3);

				String duration = result.getString(4);

				java.sql.Date release_date = result.getDate(8);

				String story_line = result.getString(9);

				String img = result.getString(7);

				Movie_On_Tips topRatedMovie = new TopRatedMovie(id, title, imdb, geners, actors, duration, release_date,

						story_line, img);

				System.out.println(topRatedMovie.getMovieData());

			}

			System.out.println();

		} else {

			System.out.println();

			System.out.println("List of top rated movies is empty");

		}



	}
}
